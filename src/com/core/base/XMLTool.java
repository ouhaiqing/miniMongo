package com.core.base;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.processors.JsonBeanProcessor;
import net.sf.json.util.JSONUtils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.test.User;

public class XMLTool {

	/**
	 * @param fileName
	 *            测试方法(打出所有数据)
	 */
	@SuppressWarnings("unchecked")
	public static void readFile(String fileName) {

		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(fileName);
			Element root = document.getRootElement();
			System.out.println(root.attributeValue("name"));
			List<Element> dbList = root.elements();
			for (Element db : dbList) {
				System.out.println(db.attributeValue("name"));
				List<Element> collectionList = db.elements();
				for (Element collection : collectionList) {
					System.out.println(collection.attributeValue("name"));
					List<Element> dataList = collection.elements();
					for (Element data : dataList) {

						String datas = data.getText();
						System.out.println(datas);
						if (datas != null && datas.length() > 0) {
							JSONObject jsonObject = JSONObject
									.fromObject(datas);
							String name = (String) jsonObject.get("name");
							System.out.println(name);
							System.out.println(jsonObject.toString());
						}

					}
					System.out.println("------------------------------------");
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 根据_ID查找相应的json数据（唯一）
	 * 
	 * @param fileName
	 * @param dbName
	 * @param collectionName
	 * @param jsonID
	 * @return
	 */
	public static String findJsonById(String fileName, String dbName,
			String collectionName, String jsonID) {
		String result = null;
		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(fileName);
			Element root = document.getRootElement();
			List<Element> dbList = root.elements();
			for (Element db : dbList) {

				if (db.attributeValue("name").equals(dbName)) {
					List<Element> collectionList = db.elements();
					for (Element collection : collectionList) {
						if (collection.attributeValue("name").equals(
								collectionName)) {
							List<Element> dataList = collection.elements();
							for (Element data : dataList) {
								String datas = data.getText();
								if (datas != null && datas.length() > 0) {
									JSONObject jsonObject = JSONObject
											.fromObject(datas);
									String id = (String) jsonObject.get("_id");
									if (jsonID.equals(id)) {
										result = jsonObject.toString();
									}

								}

							}
						}
					}
				}

			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 保存数据
	 * 
	 * @param fileName
	 * @param dbName
	 * @param collectionName
	 * @param object
	 *            保存的实体object对象
	 * @return
	 */
	public static boolean saveJson(String fileName, String dbName,
			String collectionName, Object object) {
		boolean flag = false;
		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(fileName);
			Element root = document.getRootElement();
			List<Element> dbList = root.elements();
			for (Element db : dbList) {
				if (db.attributeValue("name").equals(dbName)) {
					List<Element> collectionList = db.elements();
					//如果collection不存在，则创建
					if(collectionList.size()==0){
						Element element = DocumentHelper
								.createElement("collection");
						element.addAttribute("name", collectionName);
						collectionList.add(0, element);
					}
					for (Element collection : collectionList) {
						if (collection.attributeValue("name").equals(
								collectionName)) {

							List<Element> dataList = collection.elements();

							Element dataElement = DocumentHelper
									.createElement("data");// 该方法用来创建一个element元素
							JSONObject jsonObject = JSONObject
									.fromObject(object);
							dataElement.addText(jsonObject.toString());//
							dataList.add(0, dataElement); // 固定节点添加

							FileWriter fwFileWriter = new FileWriter(new File(
									fileName));
							XMLWriter writer = new XMLWriter(fwFileWriter);
							writer.write(document);
							writer.close();
							flag = true;

						}
					}
					
				}

			}
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 
	 * 根据_ID删除相应的json数据（唯一）
	 * 
	 * @param fileName
	 * @param dbName
	 * @param collectionName
	 * @param jsonID
	 * @return
	 */
	public static boolean deleteJsonById(String fileName, String dbName,
			String collectionName, String jsonID) {
		boolean flag = false;
		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(fileName);
			Element root = document.getRootElement();
			List<Element> dbList = root.elements();
			for (Element db : dbList) {

				if (db.attributeValue("name").equals(dbName)) {
					List<Element> collectionList = db.elements();
					for (Element collection : collectionList) {
						if (collection.attributeValue("name").equals(
								collectionName)) {
							List<Element> dataList = collection.elements();
							for (Element data : dataList) {
								String datas = data.getText();
								if (datas != null && datas.length() > 0) {
									JSONObject jsonObject = JSONObject
											.fromObject(datas);
									String id = (String) jsonObject.get("_id");
									if (jsonID.equals(id)) {
										collection.remove(data);
										FileWriter fwFileWriter = new FileWriter(
												new File(fileName));
										XMLWriter writer = new XMLWriter(
												fwFileWriter);
										writer.write(document);
										writer.close();
										flag = true;
									}

								}

							}
						}
					}
				}

			}
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 
	 * 根据_ID更新相应的json数据（唯一）
	 * 
	 * @param fileName
	 * @param dbName
	 * @param collectionName
	 * @param jsonID
	 * @return
	 */
	public static boolean updateJsonById(String fileName, String dbName,
			String collectionName, Object object) {
		boolean flag = false;
		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(fileName);
			Element root = document.getRootElement();
			List<Element> dbList = root.elements();
			for (Element db : dbList) {

				if (db.attributeValue("name").equals(dbName)) {
					List<Element> collectionList = db.elements();
					for (Element collection : collectionList) {
						if (collection.attributeValue("name").equals(
								collectionName)) {
							List<Element> dataList = collection.elements();
							for (Element data : dataList) {
								String datas = data.getText();
								if (datas != null && datas.length() > 0) {
									JSONObject jsonObject = JSONObject
											.fromObject(datas);
									JSONObject json = JSONObject
											.fromObject(object);
									String id = (String) jsonObject.get("_id");

									if (json.get("_id").equals(id)) {
										data.setText(json.toString());

										FileWriter fwFileWriter = new FileWriter(
												new File(fileName));
										XMLWriter writer = new XMLWriter(
												fwFileWriter);
										writer.write(document);
										writer.close();
										flag = true;
									}

								}

							}
						}
					}
				}

			}
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 创建数据库
	 * @param fileName
	 * @param DBname
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean createDB(String fileName,String DBname) {
		boolean flag = false;
		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(fileName);
			Element root = document.getRootElement();
			List<Element> dbList = root.elements();
			
			Element element = DocumentHelper.createElement("db");
			element.addAttribute("name", "dbTest");
			dbList.add(0, element);
			
			FileWriter fwFileWriter = new FileWriter(
					new File(fileName));
			XMLWriter writer = new XMLWriter(
					fwFileWriter);
			writer.write(document);
			writer.close();
			flag = true;
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
	

	/**
	 * 获取UUID
	 * 
	 * @return
	 */
	public static String getUUID() {

		return UUID.randomUUID().toString().toUpperCase().replace("-", "");
	}

}
